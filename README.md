# STM32_LED_MUSIC

## 介绍
基于STM32的音乐频谱灯，8x8 LED点阵

<img src="./Hardware/频谱灯.png" width="20%">

## 安装教程

1.  使用protues8软件进行仿真，采用内建的MIC和LED点阵
2.  使用CubeIDE工具进行软件的开发

## 模块链接
1.  [MAX7219点阵模块 2/4/8点阵2*2 2*4显示屏单片机控制驱动LED模块](https://item.taobao.com/item.htm?_u=62n19rn622fb&id=618274380277&spm=a1z09.2.0.0.57192e8dpbPX1U)

<img src="https://gw.alicdn.com/imgextra/i4/2658592015/O1CN017wpbOV1QkufoqkUeP_!!2658592015.jpg_Q75.jpg_.webp" width="20%">

2.  [原装正品ARM 核心板 STM32F103C8T6开发板 最小系统板 STM32](https://item.taobao.com/item.htm?_u=62n19rn6c80d&id=679227834271&spm=a1z09.2.0.0.57192e8dpbPX1U)

<img src="https://gw.alicdn.com/imgextra/i1/2658592015/O1CN01AjdF8i1QkuyCL3kBB_!!2658592015.jpg_Q75.jpg_.webp" width="20%">

3.  [声音传感器模块 声控传感器开关 声音检测 口哨开关 放大器麦克风](https://detail.tmall.com/item.htm?_u=62n19rn67fcb&id=41254382566&spm=a1z09.2.0.0.57192e8dpbPX1U)

<img src="https://gw.alicdn.com/bao/uploaded/i2/2207691322/O1CN01o8zfJ61LdWOJcs17L_!!2207691322.jpg_Q75.jpg_.webp" width="20%">



## 引脚连接说明
1.  麦克风模块OUT <--->  STM32 PA0
2.  LED点阵屏模块DIN <--->  STM32 PB7
3.  LED点阵屏模块CS <--->  STM32 PB8
4.  LED点阵屏模块CLK <--->  STM32 PB9

## 使用说明

1.  首先进行初始化操作
2.  创建定时器，将LED的刷新操作放在1ms的定时器中断里，保证刷屏的连续性
3.  主函数里采集MIC的ADC电压，然后使用arm的math库进行FFT变换，再通过数据转换映射到8bit的点阵屏上。
4.  最后进行显示
