/*
 * max7219.h
 *
 *  Created on: 2024年4月3日
 *      Author: Administrator
 */

#ifndef INC_MAX7219_H_
#define INC_MAX7219_H_

#include "main.h"

/*3个引脚的控制宏定低    位带操作*/
#define MAX7219_DIN PBout(5)
#define MAX7219_CS 	PBout(6)
#define MAX7219_CLK PBout(7)

#define MAX7219_DIN_Clr() HAL_GPIO_WritePin(MAX7219_DIN_GPIO_Port, MAX7219_DIN_Pin, GPIO_PIN_RESET)//CLK
#define MAX7219_DIN_Set() HAL_GPIO_WritePin(MAX7219_DIN_GPIO_Port, MAX7219_DIN_Pin, GPIO_PIN_SET)

#define MAX7219_LOAD_Clr() HAL_GPIO_WritePin(MAX7219_LOAD_GPIO_Port, MAX7219_LOAD_Pin, GPIO_PIN_RESET)//CLK
#define MAX7219_LOAD_Set() HAL_GPIO_WritePin(MAX7219_LOAD_GPIO_Port, MAX7219_LOAD_Pin, GPIO_PIN_SET)

#define MAX7219_CLK_Clr() HAL_GPIO_WritePin(MAX7219_CLK_GPIO_Port, MAX7219_CLK_Pin, GPIO_PIN_RESET)//CLK
#define MAX7219_CLK_Set() HAL_GPIO_WritePin(MAX7219_CLK_GPIO_Port, MAX7219_CLK_Pin, GPIO_PIN_SET)


void MAX7219_Write_byte(uint8_t data);//单字节写入
void MAX7219_Write_Command(uint8_t addr,uint8_t data);//写命令
void MAX7219_INIT(void);


#endif /* INC_MAX7219_H_ */
