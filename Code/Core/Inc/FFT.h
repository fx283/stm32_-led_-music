#ifndef _FFT_H
#define _FFT_H

#include "stm32f1xx_hal.h"
#include "stm32_dsp.h"
#include "math.h"
#include "usart.h"

#define NPT     256     // FFT Point
#define SIZE_OF_FREQ  8         // Bar size
#define FALLBACK_LATCY 1        //

extern u32 adcValues[NPT];
extern long lBufInArray[NPT];
extern long lBufOutArray[NPT/2];

void LED_Display(uint8_t i, uint8_t fftDisplayBuffer);
void Spectrum_Bounce(void);

#endif
