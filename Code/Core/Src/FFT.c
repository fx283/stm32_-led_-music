#include "FFT.h"

#include "max7219.h"

u32 adcValues[NPT]={0};
long lBufInArray[NPT];
long lBufOutArray[NPT/2];
long lBufMagArray[NPT/2];
u8 fftDisplayBuffer[SIZE_OF_FREQ];
u8 isReverse = 0;
u8 count = 0;


void GetPowerMag(void)
{
    signed short lX,lY;
    float X,Y,Mag;
    unsigned short i;
    for(i=0; i<NPT/2; i++)
    {
        lX  = (lBufOutArray[i] << 16) >> 16;
        lY  = (lBufOutArray[i] >> 16);

		
        X = NPT * ((float)lX) / 32768;
        Y = NPT * ((float)lY) / 32768;
        Mag = sqrt(X * X + Y * Y)*1.0/ NPT;
        if(i == 0)
            lBufMagArray[i] = (unsigned long)(Mag * 32768);
        else
            lBufMagArray[i] = (unsigned long)(Mag * 65536);
    }
}


int64_t map(int64_t x, int64_t in_min, int64_t in_max, int64_t out_min, int64_t out_max)
{
	return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}


void LED_Display(uint8_t i, uint8_t fftDisplayBuffer)
{
//	uint16_t GPIO_ODR = 0x0001;
	uint8_t Disp_data = 0;

	if( fftDisplayBuffer >= 0xFF )
		Disp_data = 0xFF;
	else if ( fftDisplayBuffer >= 0x7F )
		Disp_data = 0x7F;
	else if ( fftDisplayBuffer >= 0x3F )
		Disp_data = 0x3F;
	else if ( fftDisplayBuffer >= 0x1F )
		Disp_data = 0x1F;
	else if ( fftDisplayBuffer >= 0x0F )
		Disp_data = 0x0F;
	else if ( fftDisplayBuffer >= 0x07 )
		Disp_data = 0x07;
	else if ( fftDisplayBuffer >= 0x03 )
		Disp_data = 0x03;
	else
		Disp_data = 0x01;

//	Disp_data = ~Disp_data;
//	GPIO_ODR = GPIO_ODR << i;
//	GPIO_ODR = GPIO_ODR | (Disp_data<<8);
//	GPIOB->ODR = GPIO_ODR;

	MAX7219_Write_Command(8-i,Disp_data);
}


void Spectrum_Bounce(void)
{
	u16 i;
	uint8_t value = 0;
	uint8_t barIndex;
	
	for (i=0; i<NPT; i++)
	{
		lBufInArray[i] = ((signed short) (adcValues[i] - 2048)) << 16;
	}
	
	cr4_fft_256_stm32(lBufOutArray, lBufInArray, NPT);
	
	GetPowerMag();
	
	for (i = 0; i < SIZE_OF_FREQ; i++)
	{

		value = map(lBufMagArray[i * 4 + 3], 0, 128, 0, 8);
		value = value > 8 ? 8 : value;
		
		if(!isReverse)
		{
			value = (~(0xFF << value)) | 1;
			barIndex = i;
			
			if (value > fftDisplayBuffer[barIndex])
			{
				 fftDisplayBuffer[barIndex] = value;
			}
			else if (fftDisplayBuffer[barIndex] > 0x01 && (count++ > FALLBACK_LATCY))
			{
				fftDisplayBuffer[barIndex] >>= 1;
				count = 0;
			}
		}
	}

}
