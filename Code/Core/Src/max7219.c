/*
 * max7219.c
 *
 *  Created on: 2024年4月3日
 *      Author: Administrator
 */
#include "max7219.h"

void MAX7219_Write_byte(uint8_t data)//单字节写入
{
	uint8_t i=0;
	MAX7219_LOAD_Clr();//为了防止CS没有拉低，所以每次写都拉低
	for(i=8;i>0;i--)
	{
		MAX7219_CLK_Clr();
		if(data&0x80)//高位先行
		{
			MAX7219_DIN_Set();
		}
		else
		{
			MAX7219_DIN_Clr();
		}
		data=data<<1;//最高位左移以为 次高位变最高位
		MAX7219_CLK_Set();
	}
}


void MAX7219_Write_Command(uint8_t addr,uint8_t data)//写命令
{
	MAX7219_LOAD_Clr();
	MAX7219_Write_byte(addr);//寄存器地址
	MAX7219_Write_byte(data);//需要写入的数据
	MAX7219_LOAD_Set();
}


/************宏定义方便修改和观察*******************/
#define LEDCOUNT 	1///点阵屏的个数  我用的4连屏
#define DECODEMODE 		0X09	//编码模式
#define INTENSITY  		0X0A	//亮度
#define SCANLIMT		0x0B	//扫描寄存器个数
#define SHUTDOWN   		0X0C	//关闭寄存器
#define DISPLAYTEST		0X0F	//显示测试
/*************************************************/
void MAX7219_INIT(void)
{
	uint8_t i=0;
	MAX7219_LOAD_Clr();
	for(i=0;i<LEDCOUNT;i++)
	{
		MAX7219_Write_Command(DECODEMODE,0X00);
        /*译码寄存器：1使用BCD码   0不使用（数码管的话建议用BCD码）*/
	}
	MAX7219_LOAD_Set();//这里发送4次后CS拉高，那么4个点阵屏都收到数据，然后加载到寄存器中
	MAX7219_LOAD_Clr();
	for(i=0;i<LEDCOUNT;i++)
	{
		MAX7219_Write_Command(INTENSITY,0X0F);
        /*亮度控制：0x00~0x0F  0是最暗  0xFs是最亮*/
	}
	MAX7219_LOAD_Set();
	MAX7219_LOAD_Clr();
	for(i=0;i<LEDCOUNT;i++)
	{
		MAX7219_Write_Command(SCANLIMT,0X07);
        /*点阵屏的行数  数码管的段位  0是1行，7是8行*/
	}
	MAX7219_LOAD_Set();
	MAX7219_LOAD_Clr();
	for(i=0;i<LEDCOUNT;i++)
	{
		MAX7219_Write_Command(SHUTDOWN,0X01);
        /*掉电模式：0掉电模式  1正常模式*/
	}
	MAX7219_LOAD_Set();
	MAX7219_LOAD_Clr();
		for(i=0;i<LEDCOUNT;i++)
	{
		MAX7219_Write_Command(DISPLAYTEST,0X00);
        /*显示寄存器：0普通模式   1测试模式*/
	}
	MAX7219_LOAD_Set();

}



